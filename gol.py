import pygame, sys, time
from pygame.locals import *

# set up pygame
pygame.init()

boardSizePx = 800
boardCellNum = 8

# set up the window
windowSurface = pygame.display.set_mode((boardSizePx, boardSizePx), 0, 32)
pygame.display.set_caption('Hello world!')

# set up the colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

# set up fonts
basicFont = pygame.font.SysFont(None, 48)


# draw the white background onto the surface


spacing = boardSizePx / boardCellNum

def generation(cells):
    new_cells = [[False for i in range(0, len(cells[0])) ] for j in range(0, len(cells))]

    for i, row in enumerate(cells):
        for j, cell in enumerate(row):
            # import pdb; pdb.set_trace()
            neighbors = [cells[ii][jj]
                for ii in range(i - 1, i + 2)
                for jj in range(j - 1, j + 2)
                if (ii, jj) != (i, j) and ii >= 0 and ii < len(cells) and jj >=0 and jj < len(row)]
            live_neighbors = [n for n in neighbors if n]
            print(i,j,neighbors)
            if len(live_neighbors) == 3:
                new_cells[i][j] = True
            elif len(live_neighbors) == 2 and cells[i][j]:
                new_cells[i][j] = True
            else:
                new_cells[i][j] = False
    return new_cells

# # 1 cell dead need 3 neighbor cells alive
# def gol(cell)
#     if condition:
#         pass

def drawBoard(cells):
    windowSurface.fill(WHITE)
    for row in range(0, 8):
        for col in range(0, 8):
            if cells[row][col]:
                pygame.draw.rect(windowSurface, BLACK, (row * spacing, col * spacing, spacing, spacing))
    pygame.display.update()

def drawBoardGreen(cells):
    windowSurface.fill(WHITE)
    for row in range(0, 8):
        for col in range(0, 8):
            if cells[row][col]:
                pygame.draw.rect(windowSurface, GREEN, (row * spacing, col * spacing, spacing, spacing))
    pygame.display.update()

cells = [
  [True, False, True, False, True, False, True, False],
  [False, True, False, True, False, True, False, True]
] * 4

drawBoard(cells)


def addCell(row,col):
    drawCell(row,col)
    cells[row][col] = True

def drawCell(row,col):
    # draw the window onto the screen
    pygame.draw.rect(windowSurface, BLACK, (row*spacing, col*spacing, spacing, spacing))
    pygame.display.update()

# run the game loop
while True:
    for event in pygame.event.get():

        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            (i, j) = int(pos[0] / spacing), int(pos[1] / spacing)
            addCell(i,j)
            print(str(pos))

        elif event.type == pygame.KEYDOWN:
            cells = generation(cells)
            drawBoardGreen(cells)

        elif event.type == QUIT:
            pygame.quit()
            sys.exit()
